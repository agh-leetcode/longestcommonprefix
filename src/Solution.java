
public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.runTheApp();
    }

    private void runTheApp() {
        String [] input = {"flower", "flow", "flight"};
        String [] input2 = {"dog","racecar","car"};
        String [] input3 = {""};
        String [] input4 = {"c", "c"};
        String [] input5 = {};
        String [] input6 = {"abb","abc"};
        String result = longestCommonPrefix(input6);
        System.out.println(result);
    }

    private String longestCommonPrefix(String[]input){
        if (input == null || input.length==0)
            return "";
        String prefix = input[0];
        int i = 1;

        while (i<input.length){
            while (input[i].indexOf(prefix)!=0){
                prefix = prefix.substring(0, prefix.length()-1);
            }
            i++;
        }
        return prefix;
    }

    private String longestCommonPrefix2 (String [] input){
        if (input == null || input.length==0)
            return "";
        for (int i = 0; i<input[0].length();i++){
            char c = input[0].charAt(i);
            for (String s : input) {
                if (i>=s.length() || s.charAt(i) !=c)
                    return input[0].substring(0,i);
            }
        }
        return input[0];
    }

    /*private String longestCommonPrefix(String[] input) {
        if (input == null || input.length==0)
            return "";
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < input[0].length();) {
            *//*if (input[i].equals(""))
                return "";*//*
            stringBuilder.append(input[0].charAt(i));
            if (Arrays.stream(input).allMatch((s) -> s.startsWith(stringBuilder.toString()))){
                i++;
            }else {
                stringBuilder.deleteCharAt(i);
                break;
            }
        }
        if (stringBuilder.toString().equals(""))
            return "";
        else return stringBuilder.toString();
    }*/
}
